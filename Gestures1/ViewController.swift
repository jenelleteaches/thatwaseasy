//
//  ViewController.swift
//  Gestures1
//
//  Created by Jenelle Chen on 11/23/18.
//  Copyright © 2018 Jenelle Chen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var easyLabel: UILabel!
    
    var count:Int = 0
    
    @IBAction func handleTaps(_ sender: UITapGestureRecognizer) {
        print("person tapped on the image")
        
        count = count + 1
        easyLabel.text = "You pressed it \(count) times!"
    }
    
    
    @IBAction func handleDrag(_ sender: UIPanGestureRecognizer) {
        print("Person tried to drag the label")
        
        
        // how much did the person's finger move
        // get how much distance the person has moved their finger
        let distance = sender.translation(in:self.view)
        
        if (sender.view != nil) {
            let currentItem = sender.view!
            let newX = currentItem.center.x + distance.x
            let newY = currentItem.center.y + distance.y
            
            
            print("Old position: \(currentItem.center)")
            currentItem.center = CGPoint(x: newX, y: newY)
            print("New position: \(currentItem.center)")
        }
        
        sender.setTranslation(CGPoint.zero, in: self.view)
        
        print("------")
        
        
        
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

